const express = require("express");
const router = express.Router();
const Post = require("../models/Post");

router.get('/all',(req,res)=>{
  Post.find()
  .then(profiles=>{
    res.json({profile:profiles})
  })  
})

router.post("/", (req, res) => {

  Post.find({email:req.body.email})
  .then(profiles=>{
    if(profiles.length!=0){
      res.json({msg:"Email is already used"})
    }else{
      const post = new Post({
        userName:req.body.userName,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        age: req.body.age,
        password: req.body.password,
        email:req.body.email,
        status:'Active',
      });
      const savepost = post
        .save()
        .then(data => {
          res.json(data);
        })
        .catch((err) => res.json({ message: err }));
    }
  })  
  .catch(err=>{
    res.json({msg:err.message})
  })
});

router.post("/login", (req, res) => {
  Post.findOne({email:req.body.email})
  .then(user=>{
    if(user[0].password==req.body.email){
      res.json({m:"good"})
    }
    else{
      res.json({m:"bad"})
    }
})
.catch(err=>{

})
})

module.exports = router