const mongoose = require("mongoose");
const Profile = new mongoose.Schema({
  firstName: { type: String, default: "", trin: true },
  lastName: { type: String, default: "", trin: true },
  age: { type: Number, default: 0 },
  email:{type:String,default:'',train:true},
  status: { type: String, default: "active", trin: true },
  password: { type: String, default: "", train: true },
});
module.exports = mongoose.model("Profile", Profile);