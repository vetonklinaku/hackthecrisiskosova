const vertex = require('vertex360')({site_id: process.env.TURBO_APP_ID})
const express = require('express')
const PORT = process.env.PORT || 5000;
const http = require('http');
const app = express() // initialize app
const server =http.createServer(app);
const socket = require('socket.io');
const io =  socket(server)

io.on('connection', socket => {
    console.log('made socket connection', socket.id);
    socket.on('chat',data=>{
        // console.log(data);
        io.sockets.emit('chat', data);
    });
    socket.on('typing', data=>{
        socket.broadcast.emit('typing', data);
    });

});


/*  
	Apps can also be initialized with config options as shown in the commented out example below. Options
	include setting views directory, static assets directory, and database settings. To see default config
	settings, view here: https://docs.turbo360.co
*/
const config = {
	views: 'views', 							// Set views directory 
	static: 'public', 							// Set static assets directory
	db: 
	{
		url: (process.env.TURBO_ENV == 'dev') ? process.env.MONGODB_URI : process.env.PROD_MONGODB_URI,
		type: 'mongo',
		onError: (err) => {
			console.log('DB Connection Failed!')
		},
		onSuccess: () => {
			console.log('DB Successfully Connected!')
		}
	} 
}

vertex.configureApp(app, config) // remove line 30 below and replace with this

app.use(vertex.setContext(process.env))


// import routes
const members=require("./routes/members")
// set routes
app.use("/signup",members );

app.listen(PORT, () =>
  console.log(`Server is started on port: ${PORT} `)
);

module.exports = app